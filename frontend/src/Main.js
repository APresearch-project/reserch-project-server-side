import React, { Component } from "react";
import {
    Route,
    Switch
  } from "react-router-dom";
  import Admin from "./Pages/Admin/AdminPage";
  import Customer from "./Pages/Customer/CustomerPage";
  import "./Main.css"
  

  
 
class Main extends Component {
  render() {
    return (
      <Switch >
          <Route  path ="/admin" component={Admin}  />
          <Route  exact path ="/" component={Customer}  />
      </Switch>
    );
  }
}

export default Main;