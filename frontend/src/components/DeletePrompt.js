import React, {Component} from "react";
import Button from 'react-bootstrap/Button'
import Container from 'react-bootstrap/Container'
import Row from 'react-bootstrap/Row'
import Col from 'react-bootstrap/Col'
import API from './api'


export default class DeletePrompt extends Component{
   
    onButtonClick = () => {
        console.log(this.props.id)
        API.delete(`${this.props.ApiPath}${this.props.id}`)
            .then(res => {
                
                console.log(res)
                console.log(res.data)
        });
    }

    render() {
        return (
            <Container>
                 <Row>
                    <Col ><h2>Are you sure?</h2></Col>
                </Row>
                <Row>
                    <Col >
                        <Button onClick={this.onButtonClick} variant = "danger" >
                            Delete
                        </Button>
                    </Col>
                </Row>
            </Container>
           
        );
    }
}