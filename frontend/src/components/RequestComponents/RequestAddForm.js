import React, {Component} from "react";
import Form from 'react-bootstrap/Form'
import Button from 'react-bootstrap/Button'
import API from '../api'


export default class RequestAddform extends Component{
    state = {
        student_id: null,
        location: "",
        snack: "",
        snacks: []
    }
    
    handleIdChange = (event) => {
        this.setState({student_id : event.target.value});
    }

    handleLocationChange = (event) => {
        this.setState({location : event.target.value});
    }


    handleSubmit = event => {
        event.preventDefault();

        const request = {
            student_id: this.state.student_id,
            location: this.state.location,
            snacks: this.state.snacks
        };

        console.log(request)
        API
            .post('requests',request)
            .then(res => {
                
            console.log(res)
            console.log(res.data)
        });
    }

    render() {
        return (
            <Form onSubmit={this.handleSubmit}>
                <Form.Group controlId="formAddRequest">
                    <Form.Label>Student ID</Form.Label>
                    <Form.Control type="input" placeholder="Enter ID" onChange={this.handleIdChange} />
                </Form.Group>
                <Form.Group controlId="formAddRequest">
                    <Form.Label>Location</Form.Label>
                    <Form.Control type="input" placeholder="Enter Location" onChange={this.handleLocationChange} />
                </Form.Group>
                <Form.Group controlId="formAddRequest">
                    <Form.Label>Select Snack </Form.Label>
                </Form.Group>
                <Button variant="primary" type="submit">
                    Submit
                </Button>
            </Form>
        );
    }
}