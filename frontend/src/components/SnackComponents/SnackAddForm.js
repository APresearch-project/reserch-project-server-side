import React, {Component} from "react";
import Form from 'react-bootstrap/Form'
import Button from 'react-bootstrap/Button'
import API from '../api'



export default class StudentAddform extends Component{
    state = {
        image_url: "",
        name: "",
        price: "",
        quantity: 0,
    }

    handleUrlChange = (event) => {
        this.setState({image_url : event.target.value});
    }

    handleNameChange = (event) => {
        this.setState({name : event.target.value});
    }

    handlePriceChange = (event) => {
        this.setState({price : event.target.value});
    }

    handleQuantityChange = (event) => {
        this.setState({quantity : event.target.value});
    }


    handleSubmit = event => {
        event.preventDefault();

        const Snack  = {
            image_url: this.state.image_url,
            name: this.state.name,
            price: this.state.price,
            quantity: this.state.quantity,
        }

        console.log(Snack)
        API.post('snacks',Snack)
            .then(res => {
                
                console.log(res)
                console.log(res.data)
            });
    }

    render() {
        return (
            <Form onSubmit={this.handleSubmit}>
                <Form.Group controlId="FormAddSnack">
                    <Form.Label>Snack URL</Form.Label>
                    <Form.Control type="url" placeholder="Enter Snack URL" onChange={this.handleUrlChange} />
                </Form.Group>
                <Form.Group controlId="FormAddSnack">
                    <Form.Label>Name</Form.Label>
                    <Form.Control type="input" placeholder="Enter Snack Name" onChange={this.handleNameChange} />
                </Form.Group>
                <Form.Group controlId="FormAddSnack">
                    <Form.Label>Price</Form.Label>
                    <Form.Control type="number" min = "0" step = "0.01" xplaceholder="Enter Snack Price" onChange={this.handlePriceChange} />
                </Form.Group>
                <Form.Group controlId="FormAddSnack">
                    <Form.Label>Quantity</Form.Label>
                    <Form.Control type="number" min = "0" placeholder="Enter Snack Quantity" onChange={this.handleQuantityChange} />
                </Form.Group>
                <Button variant="primary" type="submit">
                    Submit
                </Button>
            </Form>
        );
    }
}