import React, {Component} from "react";
import Modal from 'react-bootstrap/Modal'
import Button from 'react-bootstrap/Button'
import ButtonToolbar from 'react-bootstrap/ButtonToolbar'



class MyVerticallyCenteredModal extends Component {
  
  render() {
    return (
      <Modal
        {...this.props}
        aria-labelledby="contained-modal-title-vcenter"
        centered
      >
        <Modal.Header closeButton>
          <Modal.Title id="contained-modal-title-vcenter">
          {this.props.heading}
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          {this.props.content}
        </Modal.Body>
      </Modal>
    );
  }
}

export default class ModalButton extends Component {
  constructor(props) {
    super(props);
    this.state = { modalShow: false };
  }

  render() {
    let modalClose = () => this.setState({ modalShow: false });
    return (
      <ButtonToolbar>
        <Button
          variant={this.props.variant}
          onClick={() => this.setState({ modalShow: true })}
        >
          {this.props.ButtonName}
        </Button>

        <MyVerticallyCenteredModal
          show={this.state.modalShow}
          onHide={modalClose}
          heading = {this.props.heading}
          content = {this.props.content}
        />
      </ButtonToolbar>
    );
  }
}