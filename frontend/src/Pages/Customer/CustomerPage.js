import React, {Component} from "react";
import Button from 'react-bootstrap/Button'
import Row from 'react-bootstrap/Row'
import Col from 'react-bootstrap/Col'
import Container from 'react-bootstrap/Container'
import {Spring} from 'react-spring/renderprops'
import {NavLink} from "react-router-dom"
import "./CustomerPage.css"
import Menu from "./Menu"
export default class student extends Component{
    constructor(props,context){
        super(props,context);

        this.state = {
            visible: false
        };

        this.handleMouseDown = this.handleMouseDown.bind(this);
        this.toggleMenu = this.toggleMenu.bind(this);
    }

    handleMouseDown(e){
        this.toggleMenu();

        console.log("clicked");
        e.stopPropagation();
    }

    toggleMenu(){
        this.setState({
            visible: !this.state.visible
        });
    }

    render () {
        return (
            <Container fluid className="page">
                <Menu handleMouseDown={this.handleMouseDown} menuVisibility={this.state.visible} />
                <Row>
                    <Col className="Oof">
                        <NavLink className="btn Oof Login-button" exact to="/admin">Log In</NavLink>    
                    </Col>
                </Row>
              
                <Row className="rowpos">
                <Spring
                    from={{ opacity: 0,position: "relative", top: "-5vh"}}
                    to={{ opacity: 1 ,position: "relative", top: "-10vh%"}}>
                    {props =>
                    <Col style={props}>
                    <Button  value="" onClick={this.toggleMenu} size = "lg" className = "CenterButton">Grab A Snack <i className="far fa-smile-wink"></i></Button>
                    </Col>
                    }
                </Spring>
                    
                </Row>
            </Container>
        );
    }
}


