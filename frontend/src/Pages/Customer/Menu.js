import React, {Component} from 'react';
import "./Menu.css";
import Form from 'react-bootstrap/Form';
import Button from 'react-bootstrap/Button'
import ButtonToolbar from 'react-bootstrap/ButtonToolbar';
import Container from 'react-bootstrap/Container'
import Col from 'react-bootstrap/Col'
import Row from 'react-bootstrap/Row'
import API from "../../components/api"
import Image from 'react-bootstrap/Image'
import Table from 'react-bootstrap/Table'


class Menu extends Component{
    _isMounted = false;

    state = {
        numberofItems: 0,
        snacks: [],
        ShoppingCartUpdated: false,
        ShoppingCart: [],
        student_ID: 0,
        student_firstName: "",
        student_lastName: " ",
        location: " ",
        cost: 0.00,
    }

    Addtocart = (snack) =>
    {
        console.log("before")
        console.log(this.state.ShoppingCart)

        let newShoppingcart = []
        
        newShoppingcart = JSON.parse(JSON.stringify(this.state.ShoppingCart));
        
        if(snack.quantity>0)
        {
            if(!newShoppingcart.find(element => element.id === snack.id ))
            {
                let newsnack = JSON.parse(JSON.stringify(snack));;
                newsnack.quantity = 1;
                newShoppingcart[newShoppingcart.length] = newsnack;
                this.setState({ShoppingCart: newShoppingcart});
                this.setState({numberofItems: this.state.numberofItems + 1});
                this.setState({ShoppingCartUpdated:true});
            }
            else
            {
                console.log("Already in Shopping cart")
            }
        }
        else
        {
            console.log("Inventory empty")
        }
        console.log(snack)
        console.log("after")
        console.log(this.state.ShoppingCart)
    }

    removefromcart = (RemovedSnack) =>
    {
        console.log("before")
        console.log(this.state.ShoppingCart)

        let newShoppingCart =this.state.ShoppingCart.filter(snack => snack.id !== RemovedSnack.id)
        this.setState({ShoppingCart: newShoppingCart});
        this.setState({numberofItems: this.state.numberofItems - 1});
        this.setState({ShoppingCartUpdated:true});

        console.log("after")
        console.log(this.state.ShoppingCart)
        
    }

    calculatecost = () => {
        let newcost = 0;
        for( let i = 0;i<this.state.ShoppingCart.length;i++)
        {
            newcost += this.state.ShoppingCart[i].price * this.state.ShoppingCart[i].quantity;
            console.log("price:" + this.state.ShoppingCart[i].price)
            console.log("Quantity:" +this.state.ShoppingCart[i].quantity)
        }
        this.setState({cost: newcost})
    }

    increaseQuantity = (snack) => {
        console.log(snack)
        let newShoppingcart = this.state.ShoppingCart
        let index = newShoppingcart.findIndex(element => element.id === snack.id)
        let Inventoryquantity = this.state.snacks.find(element => element.id === snack.id).quantity
        if(index!==-1 && snack.quantity<Inventoryquantity)
        {
            newShoppingcart[index].quantity+=1;
            this.setState({ShoppingCart:newShoppingcart})
            this.setState({ShoppingCartUpdated:true});

        }
        else
        {
            console.log("snack not found (increaseQuantity)")
        }

    }

    decreaseQuantity = (snack) => {
        console.log(snack)
        let newShoppingcart = this.state.ShoppingCart
        let index = newShoppingcart.findIndex(element => element.id === snack.id)
        if(index!==-1)
        {
            newShoppingcart[index].quantity-=1;
            if(newShoppingcart[index].quantity<1)
            {
                this.removefromcart(snack)
            
            }
            else
            {
                this.setState({ShoppingCart:newShoppingcart})
                this.setState({ShoppingCartUpdated:true});
            }     
        }
        else
        {
            console.log("snack not found (decreaseQuantity)")
        }

    }
    

    handleIdChange = (event) => {
        this.setState({student_ID : event.target.value});
    }

    handleFnameChange = (event) => {
        this.setState({student_firstName : event.target.value});
    }

    handleLnameChange = (event) => {
        this.setState({student_lastName : event.target.value});
    }

    handleLocationChange = (event) => {
        this.setState({location : event.target.value});
    }

    handleCostChange = (event) => {
        this.setState({cost : event.target.value});
    }

    handleSubmit = event => {
        event.preventDefault();

        
        const request = {
            student_ID: this.state.student_ID,
            student_firstName: this.state.student_firstName,
            student_lastName: this.state.student_lastName,
            location: this.state.location,
            cost: this.state.cost,
            snack: this.state.ShoppingCart
        };

        console.log(request)
        API
            .post('requests',request)
            .then(res => {
                
            console.log(res)
            console.log(res.data)
        });
    }

    componentDidUpdate() {
        // Typical usage (don't forget to compare props):
        console.log("ShoppingCartUpdated: " + this.state.ShoppingCartUpdated)
        if (this.state.ShoppingCartUpdated)
        {
            this.setState({ShoppingCartUpdated:false});
            this.calculatecost();

        }
    }

    componentDidMount() {
        this._isMounted = true;
        API
            .get("snacks")
            .then(res => {
                if(this._isMounted){
                    console.log(res);
                    this.setState({snacks: res.data});
                }
        });  
    }
    componentWillUnmount() {
        this._isMounted = false;
    }
    
    render() {
        var visibility = "hide";

        if(this.props.menuVisibility){
            visibility = "show";
        }

        return (
            <Container id="flyoutMenu" className = {visibility}>
                <Button id = "CancelButton" variant="danger" type="submit" onClick={this.props.handleMouseDown}>
                    Cancel Order
                </Button>
                <Row>
                    <Col id = "RequestForm" xs={5}>
                        <Form onSubmit={this.handleSubmit}>
                            <Form.Group controlId="formAddRequest">
                                <Form.Label>Student ID</Form.Label>
                                <Form.Control type="input" placeholder="Enter ID" onChange={this.handleIdChange} />
                            </Form.Group>
                            <Form.Group controlId="formAddRequest">
                                <Form.Label>First Name</Form.Label>
                                <Form.Control type="input" placeholder="Enter First Name" onChange={this.handleFnameChange} />
                            </Form.Group>
                            <Form.Group controlId="formAddRequest">
                                <Form.Label>Last Name</Form.Label>
                                <Form.Control type="input" placeholder="Enter Last Name" onChange={this.handleLnameChange} />
                            </Form.Group>
                            <Form.Group controlId="formAddRequest">
                                <Form.Label>Location</Form.Label>
                                <Form.Control type="input" placeholder="Enter Location" onChange={this.handleLocationChange} />
                            </Form.Group>
                                <Button variant="primary" type="submit">
                                    Submit
                                </Button>
                        </Form>
                    </Col>
                    <Col id = "inventory" xs={7}>
                        <Table size = "sm" striped responsive bordered hover variant="dark"  >
                            <thead >
                                <tr>
                                    <th>#</th>
                                    <th>Picture</th>    
                                    <th>Name</th>    
                                    <th>Price</th>    
                                    <th>Quantity</th>    
                                </tr>
                            </thead>
                            <tbody>
                                {this.state.snacks.map(snack => (
                                    <tr key={snack.id} >
                                        <td> {snack.id} </td>
                                        <td> <Image size ="xs" src= {snack.image_url} fluid />  </td>
                                        <td> {snack.name} </td>
                                        <td> {snack.price} </td>
                                        <td> {snack.quantity} </td>
                                        <td> 
                                            <Button onClick ={() => this.Addtocart(snack)} variant="primary" type="submit">
                                            Add to Cart
                                            </Button> 
                                        </td>
                                    </tr>
                                ))}
                            </tbody>  
                        </Table>
                    </Col>
                    <Col id="Receipt" xs={5}>
                        <div>
                           Order Details
                        </div>
                        <br />
                        <div>
                            Total Cost
                        </div>
                        <div>
                            {this.state.cost}
                        </div>
                        <div>
                            Number of Items
                        </div>
                        <div>
                            {this.state.numberofItems}
                        </div>
                                                 
                    </Col>

                    <Col id="ShoppingCart" xs={7} >
                        <Table size = "sm" striped responsive bordered hover variant="dark"  >
                            <thead >
                                <tr>
                                    <th>SHOPPING CART</th>    
                                </tr>
                            </thead>
                            <tbody>
                                {this.state.ShoppingCart.map(snack => (
                                    <tr key={snack.id} >
                                        <td> {snack.id} </td>
                                        <td> <Image size ="xs" src= {snack.image_url} fluid />  </td>
                                        <td> {snack.name} </td>
                                        <td> {snack.price} </td>
                                        <td> 
                                            {snack.quantity} 
                                            <ButtonToolbar>
                                                <Button varaint="success" onClick ={() => this.increaseQuantity(snack)}>+</Button> 
                                                <Button variant="danger"  onClick ={() => this.decreaseQuantity(snack)}>-</Button>
                                            </ButtonToolbar>
                                        </td>
                                        <td> 
                                            <Button onClick ={() => this.removefromcart(snack)} variant="danger" type="submit">
                                                Remove
                                            </Button> 
                                        </td>
                                    </tr>
                                ))}
                            </tbody>  
                        </Table>
                    </Col>
                </Row>
            </Container>
        );
    }
}

export default Menu;