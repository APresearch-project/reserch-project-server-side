import React, {Component} from "react";
import Table from 'react-bootstrap/Table'
import ButtonToolbar from 'react-bootstrap/ButtonToolbar'
import ModalButton from "../../components/ModalButton"
import DeletePrompt from "../../components/DeletePrompt"
import API from "../../components/api"


export default class Request extends Component{
    _isMounted = false
    state = {
        requests: []
    }

    componentDidMount() {
        this._isMounted = true;
        API
            .get("requests")
            .then(res => {
                if(this._isMounted){
                    console.log(res);
                    this.setState({requests: res.data});
                }
        });
    }

    checkifdelivered(isdelivered)
    {
        if(isdelivered)
        {
            return <p>Yes</p>
        }
        else
        {
            return <p>No</p>
        }
    }

    componentWillUnmount() {
        this._isMounted = false
    }
    render () {
        return (
            <div>
                {/*
                <ButtonToolbar> 
                    <ModalButton variant = "primary" ButtonName="Add Request" heading = "Add Request" 
                          content = {<RequestAddForm />} />
                </ButtonToolbar>
                */}
                <Table striped bordered hover variant="dark">
                    <thead >
                        <tr>
                            <th>#</th>
                            <th>Student ID</th>    
                            <th>First Name</th>    
                            <th>Last Name</th>    
                            <th>Location</th>    
                            <th>Snacks</th>    
                            <th>Cost</th>    
                            <th>isDelivered</th>    
                        </tr>
                    </thead>
                    <tbody>
                        {this.state.requests.map(request => (
                            <tr key={request.id} >
                                <td> {request.id} </td>
                                <td> {request.student_ID} </td>
                                <td> {request.student_firstName} </td>
                                <td> {request.student_lastName} </td>
                                <td> {request.location} </td>
                                <td> {request.snack.map(item => ( <div> {item.quantity} {item.name}</div> ))}
                                </td>
                                <td> {request.cost} </td>
                                <td> {this.checkifdelivered(request.isdelivered )}</td>
                                <td>
                                <ButtonToolbar>
                                    {/*
                                    <ModalButton variant = "success" ButtonName="Update Record" heading = "Update Request"
                                        content = {<RequestUpdateForm record = {request} />} />
                                    */}
                                    <ModalButton variant = "danger" ButtonName="Delete Record" heading = "Delete Request"
                                        content = {<DeletePrompt ApiPath = "requests/" id = {request.id}/>} />
                                </ButtonToolbar>      
                                </td>
                            </tr>
                        ))}
                    </tbody>  
                </Table>
            </div>
        );
    }
}


