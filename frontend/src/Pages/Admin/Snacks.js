import React, {Component} from "react";
import Table from 'react-bootstrap/Table'
import ButtonToolbar from 'react-bootstrap/ButtonToolbar'
import ModalButton from "../../components/ModalButton"
import SnackAddForm from "../../components/SnackComponents/SnackAddForm"
import DeletePrompt from "../../components/DeletePrompt"
import SnackUpdateForm from "../../components/SnackComponents/SnackUpdateForm"
import API from "../../components/api"
import Image from 'react-bootstrap/Image'





export default class Snack extends Component{
    _isMounted = false;

    state = {
        snacks: []
    }

    componentDidMount() {
        this._isMounted = true;
        API
            .get("snacks")
            .then(res => {
                if(this._isMounted){
                    console.log(res);
                    this.setState({snacks: res.data});
                }
        });
    }
    componentWillUnmount() {
        this._isMounted = false;
    }

    render () {
        return (
            <div>
                <ButtonToolbar> 
                    <ModalButton variant = "primary" ButtonName="Add Snack" heading = "Add Snack" 
                          content = {<SnackAddForm />} />
                </ButtonToolbar>
                
                <Table striped bordered hover variant="dark">
                    <thead >
                        <tr>
                            <th>#</th>
                            <th>Picture</th>    
                            <th>Name</th>    
                            <th>Price</th>    
                            <th>Quantity</th>    
                        </tr>
                    </thead>
                    <tbody>
                        {this.state.snacks.map(snack => (
                            <tr key={snack.id} >
                                <td> {snack.id} </td>
                                <td> <Image size ="xs" src= {snack.image_url} fluid />  </td>
                                <td> {snack.name} </td>
                                <td> {snack.price} </td>
                                <td> {snack.quantity} </td>
                                <td> 
                                <ButtonToolbar>
                                    <ModalButton variant = "success" ButtonName="Update Record" heading = "Update Snack"
                                        content = {<SnackUpdateForm record = {snack} />} />
                                    <ModalButton variant = "danger" ButtonName="Delete Record" heading = "Delete Snack"
                                        content = {<DeletePrompt ApiPath = "snacks/" id = {snack.id}/>} />
                                </ButtonToolbar>      
                                </td>
                            </tr>
                        ))}
                    </tbody>  
                </Table>
            </div>
        );
    }
}


