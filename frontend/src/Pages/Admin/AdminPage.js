import React, { Component } from "react";
import {
    Route,
    NavLink,
    Switch
  } from "react-router-dom";
  import Snacks from "./Snacks";
  import Requests from "./Requests"
 
class Main extends Component {
    constructor(props,context){
        super(props,context);

        this.state = {
            visible: false
        };

        this.handleMouseDown = this.handleMouseDown.bind(this);
        this.toggleMenu = this.toggleMenu.bind(this);
    }

    handleMouseDown(e){
        this.toggleMenu();

        console.log("clicked");
        e.stopPropagation();
    }
    toggleMenu(){
        this.setState({
            visible: !this.state.visible
        });
    }
  render() {
    return (
            <div>
                <ul className="header">
                    <li><NavLink id = "logout" exact to="/">Log Out</NavLink></li>
                    <li><NavLink to="/admin/requests">Requests</NavLink></li>
                    <li><NavLink to="/admin/snacks">Snacks</NavLink></li>
                </ul>
                
                <div className="content">
                    <Switch>
                        <Route exact path ="/admin/requests" component={Requests}  />
                        <Route exact path ="/admin/snacks" component={Snacks}  />
                    </Switch>
                </div>
            </div>
    );
  }
}

export default Main;