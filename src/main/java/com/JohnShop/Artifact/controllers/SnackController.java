package com.JohnShop.Artifact.controllers;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.JohnShop.Artifact.entities.Snack;

@CrossOrigin(origins = "https://localhost:8090/")
@RestController
@RequestMapping("/api/snacks")
public class SnackController extends BaseController<Snack>{

}
