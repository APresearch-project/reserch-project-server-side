package com.JohnShop.Artifact.controllers;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.JohnShop.Artifact.entities.Request;

@CrossOrigin(origins = "https://localhost:8090/")
@RestController
@RequestMapping("/api/requests")
public class RequestController extends BaseController<Request> {

}
