package com.JohnShop.Artifact.controllers;

import java.util.List;

import javax.validation.Valid;

import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.JohnShop.Artifact.entities.User;
import com.JohnShop.Artifact.exception.ResourceNotFoundException;
import com.JohnShop.Artifact.dtos.Userdto;
import com.JohnShop.Artifact.repositories.IUserRepository;

@CrossOrigin(origins = "https://localhost:8090/")
@RestController
@RequestMapping("/api/users")
public class UserController{

    @Autowired
    private IUserRepository repo;

    //get All users
    @GetMapping
    public List<User> getAll(){
        return repo.findAll();
    }

    //Get one user by Id
    @GetMapping(value ="{id}")
    public User get(@PathVariable(value="id") long id)
    {
        return repo.findById(id)
            .orElseThrow(() -> new ResourceNotFoundException("User Table","id",id));
    }

    //Create one User
    @PostMapping
    public User create(@Valid @RequestBody Userdto Userinfo){
        
        System.out.println(Userinfo.toString());
        User user = new User(
            Userinfo.getFirst_name(),
            Userinfo.getLast_name(),
            Userinfo.getUsername(),
            Userinfo.getPassword()
        );
        return repo.save(user);
    }

    @PutMapping(value = "{id}")
    public User update(@PathVariable(value="id") long id,
                        @Valid @RequestBody Userdto Userinfo){
        
        User finduser = repo.findById(id)
                            .orElseThrow(() -> new ResourceNotFoundException("User Table", "User id", id));
        finduser.setFirst_name(Userinfo.getFirst_name());
        finduser.setLast_name(Userinfo.getLast_name());
        
        return repo.save(finduser);
    }

    @DeleteMapping(value="{id}")
    public ResponseEntity<?> delete(@PathVariable(value="id") long id){

        User finduser = repo.findById(id)
            .orElseThrow(() -> new ResourceNotFoundException("Genric til I can define","id",id));
        
        repo.deleteById(finduser.getId());
        return new ResponseEntity<>("User was deleted",HttpStatus.OK);
    }

}