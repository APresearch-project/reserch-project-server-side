package com.JohnShop.Artifact.controllers;

import java.lang.reflect.ParameterizedType;
import java.util.List;
import com.JohnShop.Artifact.entities.BaseEntity;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.JohnShop.Artifact.exception.ResourceNotFoundException;
import com.JohnShop.Artifact.factory.BaseEntityFactory;
import com.JohnShop.Artifact.factory.EntityFactory;
import com.JohnShop.Artifact.repositories.IBaseRepository;

@CrossOrigin(origins = "https://localhost:8090/")
public class BaseController<T extends BaseEntity> {
	
	@Autowired
	private IBaseRepository<T> repo;
	
	@RequestMapping(method = RequestMethod.GET)//GetMapping Acts as a shortcut for this
	public List<BaseEntity> getAll(){
		return (List<BaseEntity>) repo.findAll();
	}
	
	@RequestMapping(method = RequestMethod.POST)
	public  T create(@Valid @RequestBody T data) {
		return (T) repo.save(data);
	}
	
	@RequestMapping(value = "{id}", method = RequestMethod.GET)
	public T get(@PathVariable(value="id")  long id) {
		return repo.findById(id)
				.orElseThrow(() -> new ResourceNotFoundException("Genric til I can define","id",id));
	}
	
	@RequestMapping(value = "{id}", method = RequestMethod.PUT)
	public T update(@PathVariable(value="id") long id,
						@Valid @RequestBody T details) {
		
		T entity = repo.findById(id)
				.orElseThrow(()-> new ResourceNotFoundException("Genric til I can define","id",id));
		
		entity = details;
		return repo.save(entity);
	}
	
	@RequestMapping(value = "{id}", method = RequestMethod.DELETE)
	public ResponseEntity<?> delete (@PathVariable(value = "id") long id){
		
		BaseEntityFactory entityFactory = new EntityFactory();//Use Factory to create object
		
		//Pass Name of Generic Class "T" to entity as a string eg "class com.johnshop.artifact.entities.snack"
		BaseEntity entity = entityFactory.createEntity( (((ParameterizedType)getClass().getGenericSuperclass()).getActualTypeArguments()[0]).toString());
		entity = repo.findById(id)
				.orElseThrow(() -> new ResourceNotFoundException("Genric til I can define","id",id));
		repo.deleteById(entity.getId());
		return ResponseEntity.ok().build();
	}
	
}
