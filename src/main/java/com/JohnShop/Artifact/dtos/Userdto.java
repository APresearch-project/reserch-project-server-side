package com.JohnShop.Artifact.dtos;

/**
 * Userdto
 */
public class Userdto {

    private String first_name;

    private String last_name;

    private String Username;

    private String Password;


    public Userdto() {
        super();
    }

    public Userdto(String first_name, String last_name, String Username, String Password) {
        this.first_name = first_name;
        this.last_name = last_name;
        this.Username = Username;
        this.Password = Password;
    }

    public String getFirst_name() {
        return this.first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getLast_name() {
        return this.last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public String getUsername() {
        return this.Username;
    }

    public void setUsername(String Username) {
        this.Username = Username;
    }

    public String getPassword() {
        return this.Password;
    }

    public void setPassword(String Password) {
        this.Password = Password;
    }

}