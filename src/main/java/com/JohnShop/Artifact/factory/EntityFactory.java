package com.JohnShop.Artifact.factory;

import com.JohnShop.Artifact.entities.*;

public class EntityFactory extends BaseEntityFactory {

	@Override
	public BaseEntity createEntity(String type) {
			BaseEntity baseEntity;
			System.out.println("The type is: " +type);
			switch(type.toLowerCase())
			{
				case "class com.johnshop.artifact.entities.snack":
					baseEntity = new Snack();
					break;
				case "class com.johnshop.artifact.entities.request":
					baseEntity = new Request();
					break;
				case "class com.johnshop.artifact.entities.user":
					baseEntity = new User();
					break;

				default: throw new IllegalArgumentException(type.toLowerCase() + " Entity does not exist in Entityfactory");
			}
			
			return baseEntity;
	}

}
