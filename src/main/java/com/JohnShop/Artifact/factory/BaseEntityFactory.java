package com.JohnShop.Artifact.factory;

import com.JohnShop.Artifact.entities.BaseEntity;

public abstract class BaseEntityFactory {
	
	public abstract BaseEntity createEntity(String type);

}
