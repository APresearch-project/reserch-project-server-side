package com.JohnShop.Artifact.entities;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name="snacks")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"}) // json config
public class Snack extends BaseEntity{
	
	private static final long serialVersionUID = 4147639779299717673L;
	
	@Column
	private String name;
	
	@Column
	private float price; 
		
	@Column
	private String Image_url;
	
	@Column
	private int quantity;
	
	@ManyToMany
	private List<Request> request;

	
	public Snack() {
		super();
	}

	public Snack(String name, float price, String image_url, int quantity, List<Request> request) {
		super();
		this.name = name;
		this.price = price;
		Image_url = image_url;
		this.quantity = quantity;
		this.request = request;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public float getPrice() {
		return price;
	}

	public void setPrice(float price) {
		this.price = price;
	}

	public String getImage_url() {
		return Image_url;
	}

	public void setImage_url(String image_url) {
		Image_url = image_url;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public List<Request> getRequest() {
		return request;
	}

	public void setRequest(List<Request> request) {
		this.request = request;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	
}
