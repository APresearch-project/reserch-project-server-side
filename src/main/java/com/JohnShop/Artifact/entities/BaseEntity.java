package com.JohnShop.Artifact.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

@MappedSuperclass
@SuppressWarnings("serial")
public abstract class BaseEntity implements Serializable {

	// IDENTITY MAKES id Unique only per type hierarchy
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO) 
	@Basic(optional = false)
	@Column(name = "id",nullable = false, columnDefinition = "BIGINT")
	
	
	private long id;
	
	public BaseEntity() {
		super();
	}
	

	public BaseEntity(long id) {
		super();
		this.id = id;
	}



		public long getId() 
		{
			return id;
		}
		
		public void setId(long id) 
		{
			this.id = id;
		}
	
}
