package com.JohnShop.Artifact.entities;

import java.security.SecureRandom;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.nio.charset.StandardCharsets;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;


@Entity
@Table(name="User")
@JsonIgnoreProperties({"hibernateLazyInitializer","handler"})
public class User extends BaseEntity {

    private static final long serialVersionUID = 6341479799792717673L;

    @Column
    private String first_name;

    @Column
    private String last_name;

    @Column
    private String Username;

    @Column
    private  byte[] hashedPassword; 
    


    public User() {
        super();
    }



    // constructor to recieve password as hash
    public User(String first_name, String last_name, String Username, byte[] hashedPassword) {
        this.first_name = first_name;
        this.last_name = last_name;
        this.Username = Username;
        this.hashedPassword = hashedPassword;
    }

    // Constructor for converting string password to Hashvalue
    public User(String first_name, String last_name, String Username, String Password) {
        this.setFirst_name(first_name);
        this.setLast_name(last_name);
        this.setUsername(Username);
        this.setHashedPassword(Password);
    }


    public String getFirst_name() {
        return this.first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getLast_name() {
        return this.last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public String getUsername() {
        return this.Username;
    }

    public void setUsername(String Username) {
        this.Username = Username;
    }

    public byte[] getHashedPassword() {
        return this.hashedPassword;
    }

    public void setHashedPassword(byte[] hashedPassword) {
        this.hashedPassword = hashedPassword;
    }

    public void setHashedPassword(String password){

        //Generate a salt
        SecureRandom random = new SecureRandom();
        byte[] salt = new byte[16];
        random.nextBytes(salt);

        try 
        {
            MessageDigest md = MessageDigest.getInstance("SHA-512");
            md.update(salt);
            this.hashedPassword = md.digest(password.getBytes(StandardCharsets.UTF_8));
        }catch(NoSuchAlgorithmException ex)
        {
             ex.printStackTrace();
        }catch(NullPointerException ex)
        {
            ex.printStackTrace();
        }       
        // KeySpec spec = new PBEKeySpec(
        //         password.toCharArray(),
        //         salt,
        //         65536, //iterations algoithm runs
        //         126
        //     );

        //     SecretKeyFactory  factory = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1");
    }
}