package com.JohnShop.Artifact.entities;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name="requests")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"}) // json config
public class Request extends BaseEntity {

	private static final long serialVersionUID = -1938567246027507296L;
	
	@Column
	private int student_ID;
	
	@Column
	private String student_firstName;
	
	@Column
	private String student_lastName;
	
	@Column
	private String location;
	
	@Column
	private boolean isDelivered = false;
		
	@Column
	private float cost;
	
	@Column
	private Date date;
	
	@ManyToMany
	@JoinColumn(name="id")
	private List<Snack> snack;

	
	
	public Request() {
		super();
	}


	public Request(int student_ID, String student_firstName, String student_lastName, String location,
			boolean isDelivered, float cost, Date date, List<Snack> snack) {
		super();
		this.student_ID = student_ID;
		this.student_firstName = student_firstName;
		this.student_lastName = student_lastName;
		this.location = location;
		this.isDelivered = isDelivered;
		this.cost = cost;
		this.date = date;
		this.snack = snack;
	}



	public Request(int student_ID, String student_firstName, String student_lastName, String location,
			 float cost, List<Snack> snack) {
		super();
		this.student_ID = student_ID;
		this.student_firstName = student_firstName;
		this.student_lastName = student_lastName;
		this.location = location;
		this.cost = cost;
		this.snack = snack;
		this.setDate();
	}

	
	
	public int getStudent_ID() {
		return student_ID;
	}

	public void setStudent_ID(int student_ID) {
		this.student_ID = student_ID;
	}

	public String getStudent_firstName() {
		return student_firstName;
	}

	public void setStudent_firstName(String student_firstName) {
		this.student_firstName = student_firstName;
	}

	public String getStudent_lastName() {
		return student_lastName;
	}

	public void setStudent_lastName(String student_lastName) {
		this.student_lastName = student_lastName;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public boolean isDelivered() {
		return isDelivered;
	}

	public void setDelivered(boolean isDelivered) {
		this.isDelivered = isDelivered;
	}

	public float getCost() {
		return cost;
	}

	public void setCost(float cost) {
		this.cost = cost;
	}

	public Date getDate() {
		return date;
	}

	public void setDate() {
		SimpleDateFormat dtf = new SimpleDateFormat("yyyy-MM-dd");
		Calendar cal = Calendar.getInstance();
		this.date = new Date();
		try {
			this.date=dtf.parse(dtf.format(( cal.get(cal.YEAR)+""+cal.get(cal.MONTH)+""+cal.get(cal.DAY_OF_MONTH))));
		} catch (ParseException e) {
			e.printStackTrace();
		}
	}

	public List<Snack> getSnack() {
		return snack;
	}

	public void setSnack(List<Snack> snack) {
		this.snack = snack;
	}


	
}