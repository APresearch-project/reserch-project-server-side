package com.JohnShop.Artifact.repositories;

import java.io.Serializable;
import org.springframework.data.jpa.repository.JpaRepository;
import com.JohnShop.Artifact.entities.BaseEntity;


public interface IBaseRepository<T extends BaseEntity> 
	extends JpaRepository<T,Serializable> {

}
