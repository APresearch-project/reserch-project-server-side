package com.JohnShop.Artifact.repositories;

import java.util.List;
import javax.validation.Valid;

import com.JohnShop.Artifact.entities.User;
import com.JohnShop.Artifact.dtos.Userdto;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.web.bind.annotation.RequestBody;

public interface IUserRepository extends JpaRepository<User, Long> {

    // public List<User> getAll();

    // public User create(@Valid @RequestBody Userdto Userinfo);
    
}
