package com.JohnShop.Artifact.repositories;

import com.JohnShop.Artifact.entities.Snack;

public interface ISnackRepository 
	extends IBaseRepository<Snack> {

}
