package com.JohnShop.Artifact.repositories;

import com.JohnShop.Artifact.entities.Request;

public interface IRequestRepository
	extends IBaseRepository<Request> {

}
