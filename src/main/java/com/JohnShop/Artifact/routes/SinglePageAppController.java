package com.JohnShop.Artifact.routes;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;



@Controller
public class SinglePageAppController {
    @RequestMapping(value = {"/admin" , "/admin/requests", "/admin/snacks"})
    public String index() {
        return "redirect:/";
    }
}