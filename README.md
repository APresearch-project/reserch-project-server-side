[![Pipeline status](https://img.shields.io/gitlab/pipeline/APresearch-project/reserch-project-server-side.svg)](https://gitlab.com/APresearch-project/reserch-project-server-side)
## Hosting
- Provider - Heroku
- Production URL - https://john-shop-production.herokuapp.com/
- Staging URL - https://john-shop-staging.herokuapp.com/

## Navigating to Admin
**Currently the app does not have an admin login.
Navigating to admin interface requires typing [http://localhost:8090/admin](http://localhost:8090/admin) in the browser**



## Tech Stack

### Java - SpringBoot
Used to create Restful API endpoints allowing the UI to perform CRUD operation on database

### React.js
Used to Create A Single Page Application.
ReactRouter used to to allow url navigation to pages
Axio used to make Restful HTTP Request to ServerSide APIs


## Tools

- Maven - version 3.6.1
- Java  - version 1.8.0_191
- jre   - version build 1.8.0_191-b12
- Node  - version v10.15.0
- npm   - version 6.4.1



## To start this app

### `Using Maven - version 3.6.1`
- run 'gitclone https://gitlab.com/ai-expert-sysrtem-project/ai-expert-system.git' in terminal
- install mvn
- Run mvn Spring-boot:run
- open a browser and go to the url [http://localhost:8090](http://localhost:8090)

### `Using Jar File`

- Download the Jar file in build folder, or clone the latest Branch and run mvn -B package
- open the terminal in the folder where you downloaded the Jar File
- Java -jar <jarnamehere>.jar (replace jarnamehere with file name)
- open a browser and go to the url [http://localhost:8090](http://localhost:8090)


